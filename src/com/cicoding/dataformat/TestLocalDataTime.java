package com.cicoding.dataformat;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Set;

import org.junit.Test;

public class TestLocalDataTime {
	
	// LocalDate、LocalTime、LocalDateTime
	@Test
	public void test1() {
		LocalDateTime localDateTime = LocalDateTime.now();
		System.out.println(localDateTime);
		
		
		LocalDateTime localDateTime1 = LocalDateTime.of(2015, 10, 19, 13, 22, 33);
		System.out.println(localDateTime1);
		
		LocalDateTime localDateTime3 = localDateTime.plusYears(2);
		System.out.println(localDateTime3);
		
		LocalDateTime localDateTime4 = localDateTime.minusMonths(2);
		System.out.println(localDateTime4);
		
		
		System.out.println(localDateTime.getYear());
		System.out.println(localDateTime.getMonthValue());
		System.out.println(localDateTime.getDayOfMonth());
		System.out.println(localDateTime.getHour());
		System.out.println(localDateTime.getMinute());
		System.out.println(localDateTime.getSecond());
		
	}
	
	//用于“时间戳”的运算。它是以Unix元年(传统
	//的设定为UTC时区1970年1月1日午夜时分)开始
	//所经历的描述进行运算
	@Test
	public void test2() {
		Instant instant = Instant.now();
		System.out.println(instant);
		
		
		OffsetDateTime dateTime = instant.atOffset(ZoneOffset.ofHours(8));
		System.out.println(dateTime);
		
		
		System.err.println(instant.toEpochMilli());
		
		Instant instant2 = Instant.ofEpochSecond(60);
		System.err.println(instant2);
		
		
	}
	
	// Duration:用于计算两个“时间”间隔
	// Period:用于计算两个“日期”间隔
	@Test
	public void test3() {
		Instant instant = Instant.now();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Instant instant2 = Instant.now();
		Duration duration = Duration.between(instant, instant2);
		System.out.println(duration.toMillis());
		
		
		System.out.println("----------------------------------------------------");
		
		LocalTime localTime = LocalTime.now();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LocalTime localTime2 = LocalTime.now();
		
		System.out.println(Duration.between(localTime, localTime2).toMillis());
		
	}
	@Test
	public void test4() {
		LocalDate date = LocalDate.of(2018, 01, 01);
		LocalDate date2 = LocalDate.now();
		
		Period period = Period.between(date, date2);
		System.out.println(period);
		
		System.out.println(period.getYears());
		System.out.println(period.getMonths());
		System.out.println(period.getDays());
		
	}
	
	//	/TemporalAdjuster : 时间校正器。有时我们可能需要获
	//	取例如：将日期调整到“下个周日”等操作。
	@Test
	public void test5() {
		LocalDateTime dateTime = LocalDateTime.now();
		System.out.println(dateTime);
		
		LocalDateTime dateTime2 = dateTime.withDayOfMonth(10);
		System.out.println(dateTime2);
		
		LocalDateTime dateTime3 = dateTime.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
		
		System.out.println(dateTime3);
		
		//自定义：下一个工作日
		LocalDateTime dateTime5 = dateTime.with((l) -> {
			LocalDateTime dateTime4 = (LocalDateTime)l;
			DayOfWeek week = dateTime4.getDayOfWeek();
			if (week.equals(DayOfWeek.FRIDAY)) {
				return dateTime4.plusDays(3);
			} else if(week.equals(DayOfWeek.SATURDAY)) {
				return dateTime4.plusDays(2);
			} else {
				return dateTime4.plusDays(1);
			}
		});
		
		System.out.println(dateTime5);
		
	}
	
	//	java.time.format.DateTimeFormatter 类：该类提供了三种
	//	格式化方法：
	//	 预定义的标准格式
	//	 语言环境相关的格式
	//	 自定义的格式
	@SuppressWarnings("static-access")
	@Test
	public void test6() {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE;
		LocalDateTime localDateTime = LocalDateTime.now();
		
		String strDate = localDateTime.format(dateTimeFormatter);
		System.out.println(strDate);
		
		System.out.println("--------------------------------");
		
		DateTimeFormatter dateTimeFormatter2 = dateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss");
		String strDate2 = dateTimeFormatter2.format(localDateTime);
		System.out.println(strDate2);
		
		LocalDateTime dateTime = localDateTime.parse(strDate2,dateTimeFormatter2);
		System.out.println(dateTime);
		
	}
	
	
	//	Java8 中加入了对时区的支持，带时区的时间为分别为：
	//	ZonedDate、ZonedTime、ZonedDateTime
	//	其中每个时区都对应着 ID，地区ID都为 “{区域}/{城市}”的格式
	//	例如 ：Asia/Shanghai 等
	//	ZoneId：该类中包含了所有的时区信息
	//	getAvailableZoneIds() : 可以获取所有时区时区信息
	//	of(id) : 用指定的时区信息获取 ZoneId 对象
	@Test
	public void test7() {
		Set<String> set = ZoneId.getAvailableZoneIds();
		set.forEach(System.out::println);
		
	}
	
	@Test
	public void test8() {
		LocalDateTime dateTime = LocalDateTime.now(ZoneId.of("Europe/Tallinn"));
		System.out.println(dateTime);
		
		LocalDateTime dateTime2 = LocalDateTime.now(ZoneId.of("Asia/Shanghai"));
		ZonedDateTime zonedDateTime = dateTime2.atZone(ZoneId.of("Asia/Shanghai"));
		System.out.println(zonedDateTime);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
