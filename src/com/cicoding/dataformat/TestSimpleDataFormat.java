package com.cicoding.dataformat;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class TestSimpleDataFormat {
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		/*Callable<Date> callable = new Callable<Date>() {
			
			@Override
			public Date call() throws Exception {
				return DateFormatThreadLocal.convert("20161218");
			}
		};
		
		ExecutorService service = Executors.newFixedThreadPool(10);
		
		List<Future<Date>> futures = new ArrayList<>();
		
		for(int i = 0; i < 10; i++){
			futures.add(service.submit(callable));
		}
		
		for (Future<Date> future : futures) {
			System.out.println(future.get());
		}
		service.shutdown();*/
		
		
		// 线程安全
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		
		Callable<LocalDate> callable = new Callable<LocalDate>() {
			
			@Override
			public LocalDate call() throws Exception {
				return LocalDate.parse("20161218", formatter);
			}
		};
		
		ExecutorService service = Executors.newFixedThreadPool(10);
		
		List<Future<LocalDate>> futures = new ArrayList<>();
		
		for(int i = 0; i < 10; i++){
			futures.add(service.submit(callable));
		}
		
		for (Future<LocalDate> future : futures) {
			System.out.println(future.get());
		}
		service.shutdown();
		
	}
	
}
