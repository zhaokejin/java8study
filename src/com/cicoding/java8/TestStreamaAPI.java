package com.cicoding.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

/**
 * @author zhaokejin 一、Stream API 的操作步骤：
 * 
 *         1. 创建 Stream
 * 
 *         2. 中间操作
 * 
 *         3. 终止操作(终端操作)
 */
public class TestStreamaAPI {

	// 1. 创建 Stream
	@Test
	public void test1() {
		// 1. Collection 提供了两个方法 stream() 与 parallelStream()
		List<String> list = new ArrayList<>();
		Stream<String> stream = list.stream(); // 获取一个顺序流
		Stream<String> parallelStream = list.parallelStream(); // 获取一个并行流

		// 2.通過Arrays中的静态方法 stream() 获取一个数组流
		Employee[] employees = new Employee[10];
		Stream<Employee> stream2 = Arrays.stream(employees);

		// 3.通过Stream类中的静态方法of()
		Stream<String> stream3 = Stream.of("aa", "bb", "cc");
		stream3.forEach(System.out::println);

		// 4.创建无限流
		// 迭代
		Stream<Integer> stream4 = Stream.iterate(0, (x) -> x + 2);
		// stream4.forEach(System.out::println);

		// 生成
		Stream<Double> stream5 = Stream.generate(Math::random).limit(2);
		stream5.forEach(System.out::println);

	}

	List<Employee> emps = Arrays.asList(new Employee(102, "李四", 59, 6666.66), new Employee(101, "张三", 18, 9999.99),
			new Employee(103, "王五", 28, 3333.33), new Employee(104, "赵六", 8, 7777.77),
			new Employee(104, "赵六", 8, 7777.77), new Employee(104, "赵六", 8, 7777.77),
			new Employee(105, "田七", 38, 5555.55));

	/*
	 * 筛选与切片 filter——接收 Lambda ， 从流中排除某些元素。 limit——截断流，使其元素不超过给定数量。 skip(n) ——
	 * 跳过元素，返回一个扔掉了前 n 个元素的流。若流中元素不足 n 个，则返回一个空流。与 limit(n) 互补
	 * distinct——筛选，通过流所生成元素的 hashCode() 和 equals() 去除重复元素
	 */

	// 内部迭代：迭代操作 Stream API 内部完成
	@Test
	public void test2() {
		// 中间操作 ； 不会执行任何操作
		Stream<Employee> filter = emps.stream().filter((e) -> {
			System.out.println("Stream API 的中间操作");
			return e.getAge() > 35;
		});
		// 终止操作 ； 所有的中间操作会一次性的全部执行，称为“惰性求值”
		filter.forEach(System.out::println);
	}

	// 外部迭代 ：
	@Test
	public void test3() {
		// 迭代操作有我们自己完成
		Iterator<Employee> it = emps.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	@Test
	public void test4() {
		emps.stream().filter((e) -> {
			System.out.println("短路！"); // && ||
			return e.getSalary() >= 5000;
		}).limit(3) // 截断流，使其元素不超过给定数量。
				.forEach(System.out::println);
	}

	@Test
	public void test5() {
		emps.parallelStream().filter((e) -> e.getSalary() > 5000).skip(2) // 跳过前两个
				.forEach(System.out::println);
	}

	@Test
	public void test6() {
		emps.stream().distinct().forEach(System.out::println);
	}

}
