package com.cicoding.java8;

import java.lang.reflect.Method;

import org.junit.Test;

/**
 * 重复注解与类型注解
 * @author zhaokejin
 *
 */
public class TestAnnotation {

	@MyAnnotation("hello")
	@MyAnnotation("world")
	public void show(@MyAnnotation("ABC") String str){
		
	}
	
	@Test
	public void test1() throws NoSuchMethodException, SecurityException{
		Class<TestAnnotation> class1 = TestAnnotation.class;
		Method method = class1.getMethod("show");
		MyAnnotation[] annotations = method.getAnnotationsByType(MyAnnotation.class);
		
		for (MyAnnotation myAnnotation : annotations) {
			System.out.println(myAnnotation.value());
		}
	}
	
}
