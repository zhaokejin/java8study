package com.cicoding.optional;

import java.security.cert.PKIXRevocationChecker.Option;
import java.util.Optional;

import org.junit.Test;

import com.cicoding.java8.Employee;

public class TestOptional {
	/**
	 * 常用方法: Optional.of(T t) : 创建一个 Optional 实例 Optional.empty() : 创建一个空的
	 * Optional 实例 Optional.ofNullable(T t):若 t 不为 null,创建 Optional 实例,否则创建空实例
	 * isPresent() : 判断是否包含值 orElse(T t) : 如果调用对象包含值，返回该值，否则返回t
	 * orElseGet(Supplier s) :如果调用对象包含值，返回该值，否则返回 s 获取的值 map(Function f):
	 * 如果有值对其处理，并返回处理后的Optional，否则返回 Optional.empty() flatMap(Function mapper):与
	 * map 类似，要求返回值必须是Optional
	 */
	@Test
	public void test1() {
		Optional<Employee> optional = Optional.of(new Employee());
		Employee employee = optional.get();
		System.out.println(employee);
	}

	@Test
	public void test2() {
		Optional<Employee> optional = Optional.empty();
		System.out.println(optional.get());

	}

	@Test
	public void test3() {
//		Optional<Employee> optional = Optional.ofNullable(new Employee());
		Optional<Employee> optional = Optional.ofNullable(null);
//		if (optional.isPresent()) {
//			System.out.println(optional.get());
//		}
		/*Employee employee = optional.orElse(new Employee(10, "张三",18,888.88));
		System.out.println(employee);*/
		optional.orElseGet(() -> new Employee());
		
	}
	
	
	@Test
	public void test4() {
		Optional<Employee> optional = Optional.ofNullable(new Employee(10, "张三",18,888.88));
		
		/*Optional<String> str = optional.map((e) -> e.getName());
		System.out.println(str.get());*/
		
		Optional<String> optional2 = optional.flatMap((e) -> Optional.of(e.getName()));		
		System.out.println(optional2.get());
		
	}

}
