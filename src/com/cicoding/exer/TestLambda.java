package com.cicoding.exer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.cicoding.java8.Employee;

public class TestLambda {
	
	List<Employee> emps = Arrays.asList(
			new Employee(101, "张三", 18, 9999.99),
			new Employee(102, "李四", 59, 6666.66),
			new Employee(103, "王五", 28, 3333.33),
			new Employee(104, "赵六", 8, 7777.77),
			new Employee(105, "田七", 38, 5555.55)
	);
	
	//1.  调用Collections.sort() 方法，通过定制排序比较两个Employee(先按年龄比，年龄相同按姓名比)，  使用Lambda 作为参数传递。。
	@Test
	public void test1(){
		Collections.sort(emps, (e1, e2) -> {
			if(e1.getAge() == e2.getAge()){
					return e1.getName().compareTo(e2.getName());
			}else{
				return -Integer.compare(e1.getAge(), e2.getAge());
			}
		});
		
		for (Employee emp : emps) {
			System.out.println(emp);
		}
	}
	
	
	//2. 1.声明函数式接口，接口中声明抽象方法，  public String getValue(String str);
	//   2.声明类TestLambda，类中编写方法使用接口作为参数，将一个字符串转换成大写，并作为方法的返回值。，
	//   3.再将一个字符串的第2 个和第4 个索引位置进行截取子串。。
	@Test
	public void test2(){
		String trimStr = strHandler("\t\t\t  练习测试   ", (str) -> str.trim());
		System.out.println(trimStr);
		
		String upper = strHandler("abcdef", (str) -> str.toUpperCase());
		System.out.println(upper);
		
		String newStr = strHandler("练习测试一二三四", (str) -> str.substring(2, 5));
		System.out.println(newStr);
	}
	
	//需求：用于处理字符串
	public String strHandler(String str, MyFunction mf){
		return mf.getValue(str);
	}
	
	
	//3.1) 声明一个带两个泛型的函数式接口，泛型类型为<T,R>  T 为参数，R 为返回值。
	// 	2) 接口中声明对应抽象方法
	//  3) 在TestLambda 类中声明方法，使用接口作为参数，计算两个long 型参数的和。。
	//  4) 再计算两个long 型参数的乘积。。
	@Test
	public void test3(){
		op(100L, 200L, (x, y) -> x + y);
		
		op(100L, 200L, (x, y) -> x * y);
	}
	
	//需求：对于两个 Long 型数据进行处理
	public void op(Long l1, Long l2, MyFunction2<Long, Long> mf){
		System.out.println(mf.getValue(l1, l2));
	}

}
