package com.cicoding.forkjoin;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.stream.LongStream;

import org.junit.Test;

public class TestForkJoin {
	
	/**
	 * ForkJoin框架
	 */
	@Test
	public void tset1(){
		Instant start = Instant.now();
		
		ForkJoinPool pool = new ForkJoinPool();
		ForkJoinTask<Long> task = new ForkJoinCalculate(0, 10000000000L);
		Long sum = pool.invoke(task);
		System.out.println(sum);
		
		Instant end = Instant.now();
		
		System.out.println("耗费时间为：" + Duration.between(start, end).toMillis());  
		
	}
	
	/**
	 * 普通 for
	 */
	@Test
	public void tset2(){
		Instant start = Instant.now();
		long sum = 0L;
		for (int i = 0; i < 10000000000L; i++) {
			sum += i;
		}
		System.out.println(sum);
		
		Instant end = Instant.now();
		
		
		System.out.println("耗费时间为：" + Duration.between(start, end).toMillis());
	}
	
	
	/**
	 * java8并行流
	 */
	@Test
	public void tset3(){
		Instant start = Instant.now();
		LongStream.rangeClosed(0, 10000000000L)
			.parallel().reduce(0,Long::sum);
		
		Instant end = Instant.now();
		
		System.out.println("耗费时间为：" + Duration.between(start, end).toMillis());
	}
	
	
	
	
	
	
	
}
