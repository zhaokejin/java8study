# java8study

#### 项目介绍
Java8新特性学习；
1. Lambda 表达式
2. 函数式接口
3. 方法引用与构造器引用
4. Stream API
5. 接口中的默认方法与静态方法
6. 新时间日期 API
7. 其他新特性

Java8新特性
什么 是函数式接口
? 1)只包含一个抽象方法的接口，称为 函数式接口。
? 2)你可以通过 Lambda 表达式来创建该接口的对象。（若 Lambda
表达式抛出一个受检异常，那么该异常需要在目标接口的抽象方
法上进行声明）。
? 3)我们可以在任意函数式接口上使用 @ @e FunctionalInterface 注解，
这样做可以检查它是否是一个函数式接口，同时 javadoc 也会包
含一条声明，说明这个接口是一个函数式接口
Java  内置四大核心函数式接口

![输入图片说明](https://gitee.com/uploads/images/2018/0518/222123_d085040f_591515.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0518/222132_235f5fa3_591515.png "屏幕截图.png")
强大的 Stream API
1了解 Stream
Java8中有两大最为重要的改变。第一个是 Lambda 表达式；另外一
个则是 Stream API( java.util.stream .*) 。
Stream 是 Java8 中处理集合的关键抽象概念，它可以指定你希望对
集合进行的操作，可以执行非常复杂的查找、过滤和映射数据等操作。
使用Stream API 对集合数据进行操作，就类似于使用 SQL 执行的数
据库查询。也可以使用 Stream API 来并行执行操作。简而言之，
Stream API 提供了一种高效且易于使用的处理数据的方式。
2.什么是 Stream
流 (Stream)  到底是什么呢 ？
是数据渠道，用于操作数据源（集合、数组等）所生成的元素序列。
“集合讲的是数据，流讲的是计算！ ”
注意：
①Stream 自己不会存储元素。
②Stream 不会改变源对象。相反，他们会返回一个持有结果的新Stream。
③Stream 操作是延迟执行的。这意味着他们会等到需要结果的时候才执行。
2.1 Stream的操作三个步骤
 1>创建  Stream
一个数据源（如：集合、数组），获取一个流
 2>中间操作
一个中间操作链，对数据源的数据进行处理
 3>终止操作( ( 终端操作) )
一个终止操作，执行中间操作链，并产生结果

![输入图片说明](https://gitee.com/uploads/images/2018/0518/222147_1f0ea9d2_591515.png "屏幕截图.png")
3.创建 Stream
Java8 中的 Collection 接口被扩展，提供了
两个获取流的方法 ：
1） default Stream<E> stream() : 返回一个顺序流
2） default Stream<E> parallelStream() : 返回一个并行流
3.1 由数组创建流
Java8 中的 Arrays 的静态方法 stream() 可
以获取数组流：
	static <T> Stream<T> stream(T[] array): 返回一个流
重载形式，能够处理对应基本类型的数组：
	public static IntStream stream(int[] array)
	public static LongStream stream(long[] array)
	public static DoubleStream stream(double[] array)
3.2由值创建流
可以使用静态方法 Stream.of(), 通过显示值
创建一个流。它可以接收任意数量的参数。
	public static<T> Stream<T> of(T... values) : 返回一个流
3.3 由函数创建流：创建无限流
可以使用静态方法 Stream.iterate() 和Stream.generate(), 创建无限流。
	迭代
public static<T> Stream<T> iterate(final T seed, final UnaryOperator<T> f)
	生成
public static<T> Stream<T> generate(Supplier<T> s) :

3.4Stream的中间操作
多个 中间操作可以连接起来形成一个 流水线，除非流水线上触发终止操作，否则 中间操作不会执行任何的 处理！而在 终止操作时一次性全部 处理，称为“惰性求值”。

	筛选与切片

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222158_c474bb3d_591515.png "屏幕截图.png")

	映射

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222202_99c42838_591515.png "屏幕截图.png")

	排序

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222207_aacc3dc8_591515.png "屏幕截图.png")

3.5Stream的终止操作
终端操作会从流的流水线生成结果。其结果可以是任何不是流的值，例如：List、Integer，甚至是 void 。

	查找与匹配

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222214_37fe871b_591515.png "屏幕截图.png")

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222224_d21d8e52_591515.png "屏幕截图.png")

	归约

![输入图片说明](https://gitee.com/uploads/images/2018/0518/222236_86332935_591515.png "屏幕截图.png")

备注：map 和 reduce 的连接通常称为 map-reduce 模式，因 Google 用它
来进行网络搜索而出名。

	收集

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222242_292cd906_591515.png "屏幕截图.png")

Collector 接口中方法的实现决定了如何对流执行收集操作(如收集到 List、Set、Map)。但是 Collectors 实用类提供了很多静态方法，可以方便地创建常见收集器实例，具体方法与实例如下表：
 
![输入图片说明](https://gitee.com/uploads/images/2018/0518/222251_d07523cb_591515.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0518/222259_64a99028_591515.png "屏幕截图.png")

3.6并行 流与 串行
并行流 就是把一个内容分成多个数据块，并用不同的线程分别处理每个数据块的流。
Java 8 中将并行进行了优化，我们可以很容易的对数据进行并行操作。Stream API 可以声明性地通过 parallel() 与sequential() 在并行流与顺序流之间进行切换。

4. Fork/Join
4.1了解  Fork/Join  框架
Fork/Join  框架：就是在必要的情况下，将一个大任务，进行拆分(fork)成若干个小任务（拆到不可再拆时），再将一个个的小任务运算的结果进行 join 汇总.

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222306_74ff39f5_591515.png "屏幕截图.png")

4.2 Fork/Join框架与传统线程池的区别
采用 “工作窃取”模式（work-stealing）：
当执行新的任务时它可以将其拆分分成更小的任务执行，并将小任务加到线程队列中，然后再从一个随机线程的队列中偷一个并把它放在自己的队列中。相对于一般的线程池实现,fork/join框架的优势体现在对其中包含的任务的处理方式上.在一般的线程池中,如果一个线程正在执行的任务由于某些原因无法继续运行,那么该线程会处于等待状态.而在fork/join框架实现中,如果某个子问题由于等待另外一个子问题的完成而无法继续运行.那么处理该子
问题的线程会主动寻找其他尚未运行的子问题来执行.这种方式减少了线程的等待时间,提高了性能
5. 新时间日期 API
	使用 LocalDate 、LocalTime 、LocalDateTime
LocalDate、LocalTime、LocalDateTime 类的实例是不可变的对象，分别表示使用 ISO-8601日历系统的日期、时间、日期和时间。它们提供了简单的日期或时间，并不包含当前的时间信息。也不包含与时区相关的信息。
注：ISO-8601日历系统是国际标准化组织制定的现代公民的日期和时间的表示法
![输入图片说明](https://gitee.com/uploads/images/2018/0518/222315_4258f5c3_591515.png "屏幕截图.png")
	Instant时间戳
用于“时间戳”的运算。它是以Unix元年(传统的设定为UTC时区1970年1月1日午夜时分)开始所经历的描述进行运算
	Duration  和 Period
	Duration:用于计算两个“时间”间隔
	Period:用于计算两个“日期”间隔
	日期的 操纵
	TemporalAdjuster : 时间校正器。有时我们可能需要获取例如：将日期调整到“下个周日”等操作。
	TemporalAdjusters : 该类通过静态方法提供了大量的常用 TemporalAdjuster 的实现。例如获取下个周日：
 
	解析 与格式化
java.time.format.DateTimeFormatter 类：该类提供了三种格式化方法：
	预定义的标准格式
	语言环境相关的格式
	自定义的格式
	时区的处理
	Java8 中加入了对时区的支持，带时区的时间为分别为：
ZonedDate、ZonedTime、ZonedDateTime
其中每个时区都对应着 ID，地区ID都为 “{区域}/{城市}”的格式
例如 ：Asia/Shanghai 等
ZoneId：该类中包含了所有的时区信息
getAvailableZoneIds() : 可以获取所有时区时区信息
of(id) : 用指定的时区信息获取 ZoneId 对象
	与传统日期处理的转换

![输入图片说明](https://gitee.com/uploads/images/2018/0518/222336_a10b326b_591515.png "屏幕截图.png")

 6.接口中的默认方法与静态方法
6.1接口中的默认方法
Java 8中允许接口中包含具有具体实现的方法，该方法称为“默认方法”，默认方法使用 default 关键字修饰。
例如：

 ![输入图片说明](https://gitee.com/uploads/images/2018/0518/222349_891cc070_591515.png "屏幕截图.png")

接口默认方法的 ” 类优先 ” 原则若一个接口中定义了一个默认方法，而另外一个父类或接口中又定义了一个同名的方法时
》选择父类中的方法。如果一个父类提供了具体的实现，那么接口中具有相同名称和参数的默认方法会被忽略。
》接口冲突。如果一个父接口提供一个默认方法，而另一个接口也提供了一个具有相同名称和参数列表的方法（不管方法是否是默认方法），那么必须覆盖该方法来解决冲突
接口 默认方法的”类优先”原则

![输入图片说明](https://gitee.com/uploads/images/2018/0518/222355_881d2703_591515.png "屏幕截图.png")

6.2接口中的静态方法
Java8 中，接口中允许添加静态方法。
例如： 

![输入图片说明](https://gitee.com/uploads/images/2018/0518/222359_5a66cc86_591515.png "屏幕截图.png")

7. 其他新特性
	Optional 类
Optional<T> 类(java.util.Optional) 是一个容器类，代表一个值存在或不存在，
原来用 null 表示一个值不存在，现在 Optional 可以更好的表达这个概念。并且
可以避免空指针异常。
常用方法：
Optional.of(T t) : 创建一个 Optional 实例
Optional.empty() : 创建一个空的 Optional 实例
Optional.ofNullable(T t):若 t 不为 null,创建 Optional 实例,否则创建空实例
isPresent() : 判断是否包含值
orElse(T t) : 如果调用对象包含值，返回该值，否则返回t
orElseGet(Supplier s) :如果调用对象包含值，返回该值，否则返回 s 获取的值
map(Function f): 如果有值对其处理，并返回处理后的Optional，否则返回 Optional.empty()
flatMap(Function mapper):与 map 类似，要求返回值必须是Optional

	重复注解与类型注解
Java 8对注解处理提供了两点改进：可重复的注解及可用于类型的注解。

![输入图片说明](https://gitee.com/uploads/images/2018/0518/222409_2b2c6a9f_591515.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0518/222413_d6f05385_591515.png "屏幕截图.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0518/222418_701723a3_591515.png "屏幕截图.png")
